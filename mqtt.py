#! /usr/bin/python3
#Droits réservés aux élèves de l'université de Bordeaux : 
#Marion Faidy et Marie Castagnet


import socket
import logging
import sys
import os
import traceback
import select
from sys import stdin
import time

PORT = 1883

PROTOCOL_NAME = "MQTT"
PROTOCOL_VERSION = 0x4
QOS_DEFAULT = 0
TYPE_CONNECT = 0x10
TYPE_CONNACK = 0x20
TYPE_DISCONNECT = 0xe0
TYPE_PUBLISH = 0x30
TYPE_SUBSCRIBE = 0x80
TYPE_SUBACK = 0x90
TYPE_CONTROLEFIELD = 0x2
PACKET_ID = 0x0001


# fonction qui renvoi le premier octet d'une trame, permet donc de déterminer
# de quel type de trame il s'agit.
def frameDecode(frame) :
    msg_type = frame[:1]
    return msg_type

# fonction qui retourne la value d'une trame publish
def value_decodePub(frame) :
    length = frame[1:2]
    int_length = int.from_bytes(length, byteorder  = 'big')
    lengthTop = frame[3:4]
    int_lengthTop = int.from_bytes(lengthTop, byteorder='big')
    len_value = int_length - int_lengthTop - 2
    value = frame[int_length - len_value +2 : int_length + 2]
    return value.decode('utf-8')

# fonction qui retourne le topic d'une trame publish
def decode_topicPub(frame):
    length = frame[1:2]
    int_length = int.from_bytes(length, byteorder='big')
    len_top = frame[3:4]
    int_len_top = int.from_bytes(len_top, byteorder='big')
    len_value = int_length - int_len_top -2
    topic = frame[int_length - int_len_top - len_value +2 : int_length -len_value +2]
    return topic.decode('utf-8')

# fonction qui retourne le topic d'une trame subscribe
def decode_topicSub (frame):
    length = frame[1:2]
    int_length = int.from_bytes(length, byteorder='big')
    len_top = frame[5:6]
    int_len_top = int.from_bytes(len_top, byteorder='big')
    topic =frame [int_length - int_len_top +1: int_length +1 ]
    return topic.decode('utf-8')

# fonction qui retourne le topic d'une trame connect
def clientid_decodeConnect(frame):
    length = frame[1:2]
    int_length=int.from_bytes(length, byteorder ='big')
    length_id= frame[12:13]
    int_length_id=int.from_bytes(length_id, byteorder='big')
    clientId=frame[int_length-int_length_id +2:int_length+2]
    return clientId


def create_mqtt_publish_msg(topic, value, retain=False):
    """ 
    Creates a mqtt packet of type PUBLISH with DUP Flag=0 and QoS=0.
    >>> create_mqtt_publish_msg("temperature", "45", False).hex(" ")
    '30 0f 00 0b 74 65 6d 70 65 72 61 74 75 72 65 34 35'
    >>> create_mqtt_publish_msg("temperature", "45", True).hex(" ")
    '31 0f 00 0b 74 65 6d 70 65 72 61 74 75 72 65 34 35'
    """
    retain_code = 0
    if retain:
        retain_code = 1
    # 0011 0000 : Message Type = Publish ; Dup Flag = 0 ; QoS = 0
    msg_mqtt_flags = (TYPE_PUBLISH + retain_code).to_bytes(1, byteorder='big')
    msg_topic = topic.encode("ascii")
    msg_value = bytes(value, "ascii")
    msg_topic_length = len(msg_topic).to_bytes(2, byteorder='big')
    msg = msg_topic_length + msg_topic + msg_value
    msg_length = len(msg).to_bytes(1, byteorder='big')

    return msg_mqtt_flags + msg_length + msg


def create_mqtt_connect_msg(clientID):
    """
    Creates a mqtt packet of type CONNECT with DUP Flag=0 and QoS=0.
    >>> create_mqtt_connect_msg("MQTT", "4", "titi", "60").hex(" ")
    '10 31 36 00 04 4d 51 54 54 04 02 00 3C 00 04 74 69 74 69'
    """

    # 0001 0000 : Message Type = Connect ; Dup Flag = 0 ; QoS = 0
    msg_mqtt_flags = TYPE_CONNECT.to_bytes(1, byteorder='big')
    msg_protocoleName = PROTOCOL_NAME.encode("ascii")
    msg_protocole_length = len(msg_protocoleName).to_bytes(2, byteorder='big')
    msg_level = PROTOCOL_VERSION.to_bytes(1, byteorder='big')
    keepalive = 60
    msg_keepAlive = keepalive.to_bytes(1, byteorder='big')
    msg_clientID = clientID.encode("ascii")
    flag = 2
    msg_flagConnect = flag.to_bytes(1,byteorder='big')
    msg_clientID_length = len(msg_clientID).to_bytes(2, byteorder='big')
    msg = msg_protocole_length + msg_protocoleName + msg_level + msg_flagConnect + msg_keepAlive + msg_clientID_length + msg_clientID
    msg_length = len(msg).to_bytes(1, byteorder='big')

    return msg_mqtt_flags + msg_length + msg


def create_mqtt_subscribe_msg (topic) :

    # 1000 0010 : Message Type = Connect ; Dup Flag = 0 ; QoS = 0

    msg_mqtt_flags = (TYPE_SUBSCRIBE + TYPE_CONTROLEFIELD).to_bytes(1, byteorder='big')
    msg_packet = PACKET_ID.to_bytes(2, byteorder ='big')
    msg_topic = topic.encode("ascii")
    msg_topic_length = len(msg_topic).to_bytes(2, byteorder='big')
    msg = msg_packet + msg_topic_length + msg_topic + QOS_DEFAULT.to_bytes(1, byteorder='big')
    
    msg_length = len(msg).to_bytes(1, byteorder='big')

    message = msg_mqtt_flags + msg_length + msg
    return message


def create_mqtt_disconnect_msg():
    msg_mqtt_flags = (TYPE_DISCONNECT).to_bytes(2,byteorder = 'little')
    return msg_mqtt_flags


def create_mqtt_suback_msg(topic, packet_id):
    msg = TYPE_SUBACK.to_bytes(1,byteorder = 'big')
    msg_len = (3).to_bytes(1, byteorder = 'big')
    msg_id= (1).to_bytes(2, byteorder = 'big')
    msg_code= (0).to_bytes(1, byteorder = 'big')
    msg = msg + msg_len + msg_id + msg_code
    return msg

def create_mqtt_connack_msg():
    msg_mqqt_flags = (TYPE_CONNACK).to_bytes(1, byteorder = 'big')
    msg_remaining = (2).to_bytes(1, byteorder= 'big')
    msg_flag = (2).to_bytes(1, byteorder = 'big')
    msg_returnCode = (0).to_bytes(1, byteorder='big')
    msg = msg_mqqt_flags + msg_remaining + msg_flag  + msg_returnCode 
    return msg

def run_publisher(addr, topic, pub_id, retain=False):
    #création de la socket
    s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

    try:
        s.connect(addr)
    except Exception as e :
        print(e)
        sys.exit(1)

    con = create_mqtt_connect_msg(pub_id)
    s.sendall(con)

    reponse = s.recv(1024)
    reponse_connack = create_mqtt_connack_msg()
    if reponse != reponse_connack:
        s.close()

    while True:
        # lecture sur l'entrée standard
        for line in stdin:
            value = line[:-1]
            msg_publish = create_mqtt_publish_msg(topic, value, False)
            s.sendall(msg_publish)
            time.sleep(0.2)

            if len(value)==0 :
                disconnection = create_mqtt_disconnect_msg()
                s.sendall(disconnection)
                print("client publisher deconnection")
                s.close()
                break

    
def run_subscriber(addr,topic,sub_id):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

    try:
        s.connect(addr)
    except Exception as e :
        print(e)
        sys.exit(1)

    connection = create_mqtt_connect_msg(sub_id)
    s.sendall(connection)

    reponse = s.recv(127)
    reponse_connack = create_mqtt_connack_msg()
    if reponse != reponse_connack:
        s.close()

    subscribe = create_mqtt_subscribe_msg(topic)
    s.sendall(subscribe)
    
    reponse_suback = create_mqtt_suback_msg(topic, PACKET_ID)
    reponse = s.recv(127)

    if reponse != reponse_suback :
        disconnection = create_mqtt_disconnect_msg()
        s.sendall(disconnection)
        s.close()

    while True :
        data = s.recv(127)
        reponse_value = value_decodePub(data)
        
        if len(reponse_value)==0:
            print("deconnection")
            disconnection = create_mqtt_disconnect_msg()
            s.sendall(disconnection)
            s.close()
            break

        else :
            print(reponse_value)


def run_server(addr):
    server = socket.socket(socket.AF_INET6 , socket.SOCK_STREAM,0)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
    
    server.bind(addr)
    server.listen(1)

    listeSockets=[]

    listeClients = []

    while True:
        r, _, _ = select.select(listeSockets + [server], [], [])
        for s2 in r:
            if s2 == server:
                s3, a = server.accept()
                data = s3.recv(127)
                if frameDecode(data) == b'\x10' : # connect
                    s3.sendall(create_mqtt_connack_msg())
                    print("new client:", a)
                    listeSockets = listeSockets + [s3]

            else:
                msg = s2.recv(127)

                if frameDecode(msg) == b'\x82': # subscribe
                    print("mode subscribe")
                    topicSub = decode_topicSub(msg)
                    listeClients = listeClients + [(s2, topicSub)]
                    s2.sendall(create_mqtt_suback_msg(topicSub, PACKET_ID))

                elif frameDecode(msg) == b'0': #publish
                    print("mode publish")
                    topicPub = decode_topicPub(msg)
                    print("msg", msg)

                    for i in listeClients :
                        socketCli, topic = i 
                        if topic == topicPub:
                            socketCli.sendall(msg)

                elif frameDecode(msg) == b'\xe0' : #disconnect
                    for i in listeClients:
                        socketCli, topic = i
                        if socketCli == s2:
                            listeClients.remove(i)
                    s2.close()
                    listeSockets.remove(s2)
                    break